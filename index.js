const Event = require('./lib/Event');
const EventHolder = require('./lib/EventHolder');
const EventBus = require('./lib/EventBus');

const eventBus = new EventBus(Event, EventHolder);

module.exports = eventBus;
