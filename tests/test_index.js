const eventBus = require('../index');
const EventBusClass = require('../lib/EventBus');
const assert = require('assert');

describe('EventBus', () => {
    let test, subTest, subTestTest, subTest2, subSubTest;
    before(() => {
        test = false;
        eventBus.addListener('test', () => {
            test = true;
        });
        eventBus.addListener('sub:test', () => {
            subTest = true;
        });
        eventBus.addListener('sub:test2', () => {
            subTest2 = true;
        });
        eventBus.addListener('sub:sub:test', () => {
            subSubTest = true;
        });
        eventBus.addListener('sub:test:test', () => {
            subTestTest = true;
        });
    });
    beforeEach(() => {
        test = false;
        subTest = false;
        subTest2 = false;
        subSubTest = false;
        subTestTest = false;
    });
    it('Should be an evenBus', () => {
        assert.ok(eventBus instanceof EventBusClass);
    });
    it('Should emit a test event', () => {
        eventBus.emit('test');
        assert.ok(test);
        assert.strictEqual(subTest, false);
        assert.strictEqual(subTest2, false);
        assert.strictEqual(subSubTest, false);
        assert.strictEqual(subTestTest, false);
    });
    it('Should emit an event on all sub events', () => {
        eventBus.emit('sub:**');
        assert.strictEqual(test, false);
        assert.strictEqual(subTest, true);
        assert.strictEqual(subTest2, true);
        assert.strictEqual(subSubTest, false);
        assert.strictEqual(subTestTest, false);
    });
    it('Should emit an event on all sub events', () => {
        eventBus.emit('sub:**:test');
        assert.strictEqual(test, false);
        assert.strictEqual(subTest, false);
        assert.strictEqual(subTest2, false);
        assert.strictEqual(subSubTest, true);
        assert.strictEqual(subTestTest, true);
    });
    it('Should emit all sub events', () => {
        eventBus.emit('sub:***');
        assert.strictEqual(test, false);
        assert.strictEqual(subTest, true);
        assert.strictEqual(subTest2, true);
        assert.strictEqual(subSubTest, true);
    });
    it('Should emit all sub events', () => {
        eventBus.emit('***');
        assert.strictEqual(test, true);
        assert.strictEqual(subTest, true);
        assert.strictEqual(subTest2, true);
        assert.strictEqual(subSubTest, true);
    });
});
