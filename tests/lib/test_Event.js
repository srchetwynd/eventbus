const Event = require('../../lib/Event');
const assert = require('assert');

describe('lib/Event', () => {
    it('Should return an instance of an Event', () => {
        const res = new Event('name', () => []);

        assert.ok(res instanceof Event);
    });
    it('Should error if name is not a string', () => {
        const test = () => new Event(5, () => []);

        assert.throws(test, {
            name: 'TypeError',
            details: ['eventName must be a string got: number'],
        });
    });
    it('Should throw if callback is not a function', () => {
        const test = () => new Event('name', 5);

        assert.throws(test, {
            name: 'TypeError',
            details: ['callback must be a function got: number'],
        });
    });
    it('Should return an array of error details', () => {
        const test = () => new Event(5, 5);

        assert.throws(test, {
            name: 'TypeError',
            details: [
                'eventName must be a string got: number',
                'callback must be a function got: number',
            ],
        });
    });
    describe('getPath', () => {
        it('Should return the path', () => {
            const event = new Event('test', () => []);
            const res = event.eventPath;

            assert.ok(Array.isArray(res));
            assert.deepStrictEqual(res, ['test']);
        });
        it('Should return an array split by ":"', () => {
            const event = new Event('test:test2:test3', () => []);
            const res = event.eventPath;

            assert.ok(Array.isArray(res));
            assert.deepStrictEqual(res, ['test', 'test2', 'test3']);
        });
    });
    describe('eventName', () => {
        it('Should return the event name', () => {
            const event = new Event('test', () => []);
            const res = event.eventName;

            assert.strictEqual(typeof res, 'string');
            assert.strictEqual(res, 'test');
        });
        it('Should return the event name', () => {
            const event = new Event('test:test2:test3', () => []);
            const res = event.eventName;

            assert.strictEqual(typeof res, 'string');
            assert.strictEqual(res, 'test:test2:test3');
        });
    });
    describe('callback', () => {
        it('Should get the callback', () => {
            const callback = (a) => a.toUpperCase();
            const res = new Event('test', callback);

            assert.strictEqual(res.callback, callback);
        });
    });
    describe('counter', () => {
        beforeEach(() => {
            Event._counter = 0;
        });
        it('Should increment _counter when getting counter', () => {
            assert.strictEqual(Event._counter, 0);

            assert.strictEqual(Event.counter, 1);
            assert.strictEqual(Event.counter, 2);
            assert.strictEqual(Event.counter, 3);
        });
        it('Should set increment the counter for each Event', () => {
            assert.strictEqual(Event._counter, 0);

            new Event('test', () => []);
            assert.strictEqual(Event._counter, 1);

            new Event('test', () => []);
            assert.strictEqual(Event._counter, 2);

            new Event('test', () => []);
            assert.strictEqual(Event._counter, 3);
        });
    });
    describe('id', () => {
        beforeEach(() => {
            Event._counter = 0;
        });
        it('Should get the id', () => {
            const event = new Event('test', () => []);
            const res = event.id;

            assert.strictEqual(res, 1);
        });
    });
    describe('removeCallback', () => {
        it('should get the remove callback', () => {
            const callback = () => [];
            const event = new Event('test', () => []);
            event._remove = callback;

            assert.strictEqual(event.removeCallback, callback);
        });
        it('should set the remove callback', () => {
            const callback = () => [];
            const event = new Event('test', () => []);
            event.removeCallback = callback;

            assert.strictEqual(event._remove, callback);
        });
    });
    describe('remove', () => {
        beforeEach(() => {
            Event._counter = 0;
        });
        it('Should call the remove callback', () => {
            let called = false;
            let calledWith = null;
            const callback = (val) => {
                called = true;
                calledWith = val;
            };
            const event = new Event('test', () => []);
            event._remove = callback;
            event.remove();

            assert.ok(called);
            assert.strictEqual(calledWith.id, 1);
            assert.strictEqual(calledWith.eventName, 'test');
        });
    });
    describe('toString', () => {
        beforeEach(() => {
            Event._counter = 0;
        });
        it('Should stringify the event', () => {
            const event = new Event('test', () => []);
            const res = event.toString();

            assert.strictEqual(
                res,
                '{"id":1,"eventName":"test","callback":"() => []"}'
            );
        });
    });
});
