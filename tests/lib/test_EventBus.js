const EventBus = require('../../lib/EventBus');
const assert = require('assert');
const Event = require('../../lib/Event');
const EventHolder = require('../../lib/EventHolder');

describe('EventBus', () => {
    it('Should create a new event bus', () => {
        const res = new EventBus(Event, EventHolder);
        assert.ok(res instanceof EventBus);
    });
    it('Should error if Event is not passed in', () => {
        const test = () => new EventBus(undefined, EventHolder);

        assert.throws(test, {
            name: 'TypeError',
            details: ['Expected Event to be passed in'],
        });
    });
    it('Should error if EventHolder is not passed in', () => {
        const test = () => new EventBus(Event, undefined);

        assert.throws(test, {
            name: 'TypeError',
            details: ['Expected EventHolder to be passed in'],
        });
    });
    describe('addListener', () => {
        it('Should add a new eventListener', () => {
            const bus = new EventBus(Event, EventHolder);
            const res = bus.addListener('test', () => []);

            assert.ok(res instanceof Event);
            assert.strictEqual(bus._events._subEvents.test._callbacks[0], res);
        });
    });
    describe('removeListener', () => {
        it('Should remove a listener', () => {
            const bus = new EventBus(Event, EventHolder);
            const event = bus.addListener('test', () => []);

            assert.strictEqual(
                bus._events._subEvents.test._callbacks[0],
                event
            );

            bus.removeListener(event);
            assert.strictEqual(
                typeof bus._events._subEvents.test._callbacks[0],
                'undefined'
            );
        });
    });
    describe('emit', () => {
        it('Should emit an event on the bus', () => {
            let called = false;
            const bus = new EventBus(Event, EventHolder);
            bus.addListener('test', () => {
                called = true;
            });

            bus.emit('test');
            assert.strictEqual(called, true);
        });
        it('Should emit an event with data', () => {
            let data = null;
            const bus = new EventBus(Event, EventHolder);
            bus.addListener('test', (inData) => {
                data = inData;
            });

            bus.emit('test', 'This was passed in the emit');
            assert.strictEqual(data, 'This was passed in the emit');
        });
    });
});
