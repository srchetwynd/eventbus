const EventHolder = require('../../lib/EventHolder');
const assert = require('assert');

describe('lib/EventHolder', () => {
    it('Should retrun an instance of EventHolder', () => {
        const res = new EventHolder();

        assert.ok(res instanceof EventHolder);
    });
    describe('callbacks', () => {
        it('Should return the callbacks', () => {
            const holder = new EventHolder();
            holder._callbacks.push('test');
            const res = holder.callbacks;

            assert.deepStrictEqual(res, ['test']);
        });
    });
    describe('subEvents', () => {
        it('Should get the sub events', () => {
            const holder = new EventHolder();
            holder._subEvents.test = 'cat';
            const res = holder.subEvents;

            assert.deepStrictEqual(res, {
                test: 'cat',
            });
        });
    });
    describe('allSubCallbacks', () => {
        it('Should get all subEvents', () => {
            const holder = new EventHolder();
            holder._subEvents.test1 = new EventHolder();
            holder._subEvents.test1._callbacks.push('test1');
            holder._subEvents.test2 = new EventHolder();
            holder._subEvents.test2._callbacks.push('test2');
            holder._subEvents.test2._subEvents.test3 = new EventHolder();
            holder._subEvents.test2._subEvents.test3._callbacks.push('test3');
            const res = holder.allSubCallbacks;

            assert.deepStrictEqual(res, ['test1', 'test2', 'test3']);
        });
    });
    describe('getSubEvent', () => {
        it('Should get the passed subevent', () => {
            const holder = new EventHolder();
            const subHolder = new EventHolder();
            holder._subEvents.test1 = subHolder;
            const res = holder.getSubEvent('test1');

            assert.deepStrictEqual(res, subHolder);
        });
    });
    describe('getCallbacks', () => {
        it('Should get callbacks on current object', () => {
            const holder = new EventHolder();
            holder._callbacks.push('test');
            holder._callbacks.push('test2');
            const res = holder.getCallbacks([]);

            assert.deepStrictEqual(res, ['test', 'test2']);
        });
        it('Should get callbacks from subHolders in a holder', () => {
            const holder = new EventHolder();
            const holder2 = new EventHolder();
            const holder3 = new EventHolder();
            holder._callbacks.push('holdertest');
            holder2._callbacks.push('holder2test');
            holder3._callbacks.push('holder3test');
            holder._subEvents.test2 = holder2;
            holder._subEvents.test3 = holder3;
            const res = holder.getCallbacks(['**']);

            assert.deepStrictEqual(res, ['holder2test', 'holder3test']);
        });
        it('Should get callbacks from all subHolders', () => {
            const holder = new EventHolder();
            const holder2 = new EventHolder();
            const holder3 = new EventHolder();
            holder._callbacks.push('holdertest');
            holder2._callbacks.push('holder2test');
            holder3._callbacks.push('holder3test');
            holder._subEvents.test2 = holder2;
            holder2._subEvents.test3 = holder3;
            const res = holder.getCallbacks(['***']);

            assert.deepStrictEqual(res, ['holder2test', 'holder3test']);
        });
        it('Should get callbacks from a subHolder', () => {
            const holder = new EventHolder();
            const holder2 = new EventHolder();
            const holder3 = new EventHolder();
            holder._callbacks.push('holdertest');
            holder2._callbacks.push('holder2test');
            holder3._callbacks.push('holder3test');
            holder._subEvents.test2 = holder2;
            holder._subEvents.test3 = holder3;
            const res = holder.getCallbacks(['test2']);

            assert.deepStrictEqual(res, ['holder2test']);
        });
        it('Should return an empty array if path does not exist', () => {
            const holder = new EventHolder();
            const res = holder.getCallbacks(['test2']);

            assert.deepStrictEqual(res, []);
        });
    });
    describe('addCallback', () => {
        it('Should add a callback to the holder', () => {
            const holder = new EventHolder();
            const event = { name: 'foo' };
            holder.addCallback(event);

            assert.strictEqual(holder._callbacks[0], event);
        });
        it('Should add a the reomove function to the event', () => {
            const holder = new EventHolder();
            const event = { name: 'foo' };
            holder.addCallback(event);

            assert.strictEqual(event.removeCallback, holder._removeCallback);
        });
    });
    describe('addSubEvent', () => {
        it('Should add a sub holder', () => {
            const holder = new EventHolder();
            holder.addSubEvent('test');

            assert.ok(holder._subEvents.test instanceof EventHolder);
        });
        it('Should not re-add a subEvent if it already exists', () => {
            const holder = new EventHolder();
            const test = { test: 'test' };
            holder._subEvents.test = test;
            holder.addSubEvent('test');

            assert.strictEqual(holder._subEvents.test, test);
        });
    });
    describe('add', () => {
        it('should add a new subEvent', () => {
            const holder = new EventHolder();
            holder.add(['test'], { test: 'test' });

            assert.ok(holder._subEvents.test instanceof EventHolder);
            assert.deepStrictEqual(holder._subEvents.test._callbacks[0], {
                removeCallback: holder._removeCallback,
                test: 'test',
            });
        });
        it('Should add a path of sub event', () => {
            const holder = new EventHolder();
            holder.add(['test', 'test2', 'test3'], { test: 'test' });

            assert.ok(holder._subEvents.test instanceof EventHolder);
            assert.ok(
                holder._subEvents.test._subEvents.test2 instanceof EventHolder
            );
            assert.ok(
                holder._subEvents.test._subEvents.test2._subEvents
                    .test3 instanceof EventHolder
            );
            assert.deepStrictEqual(
                holder._subEvents.test._subEvents.test2._subEvents.test3
                    ._callbacks[0],
                {
                    removeCallback: holder._removeCallback,
                    test: 'test',
                }
            );
        });
    });
    describe('remove', () => {
        it('Should remove a callback from the current holder', () => {
            const holder = new EventHolder();
            const event = {
                removeCallback: holder._removeCallback,
                id: 1,
            };
            holder._callbacks.push(event);
            holder.remove([], event);

            assert.strictEqual(holder._callbacks.length, 0);
        });
        it('Should remove the correct callback', () => {
            const holder = new EventHolder();
            const event1 = {
                removeCallback: holder._removeCallback,
                id: 1,
            };
            const event2 = {
                removeCallback: holder._removeCallback,
                id: 2,
            };
            const event3 = {
                removeCallback: holder._removeCallback,
                id: 3,
            };
            holder._callbacks.push(event1);
            holder._callbacks.push(event2);
            holder._callbacks.push(event3);
            holder.remove([], event2);

            assert.strictEqual(holder._callbacks.length, 2);
            assert.strictEqual(holder._callbacks[0].id, 1);
            assert.strictEqual(holder._callbacks[1].id, 3);
        });
        it('Should remove a callback from a sub event', () => {
            const holder = new EventHolder();
            const holder2 = new EventHolder();
            holder._subEvents.test1 = holder2;
            const event = {
                removeCallback: holder2._removeCallback,
                id: 1,
            };
            holder2._callbacks.push(event);
            holder.remove(['test1'], event);

            assert.strictEqual(holder2._callbacks.length, 0);
        });
        it('Should not error if sub event does not exist', () => {
            const holder = new EventHolder();
            const test = () => holder.remove(['test'], { id: 1 });

            assert.doesNotThrow(test);
        });
    });
});
