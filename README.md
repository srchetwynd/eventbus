# eventBus
![](https://gitlab.com/srchetwynd/eventbus/badges/master/coverage.svg) ![](https://gitlab.com/srchetwynd/eventbus/badges/master/pipeline.svg) ![](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)

A simple event bus, register event listeners and emit events on the bus.

## Installation

To install use:

```
npm install @srchetwynd/eventbus
```

## Testing

To run unit tests use:

```
npm run test
```

## Usage

### Importing

The event bus is a singleton, so it can be imported into multiple files and use
each file will use the same instance;


```
const eventBus = require('@srchetwynd/eventbus');
```

### Listening

To add a listener to the bus simple call the addListener function. The function
takes two parameters, the name of the event to listen to, and the callback to be
ran when an event is emitted.

```
eventBus.addListener('someEvent', (val, event) => {
    console.log('This was emited:', val);
});
```

The callback is called with the value which was emitted, and the event object.

Sub events can be emitted to, this is useful if multiple event have the same
name but its required to keep track of where it was emitted from. To listen to a
subevent enter the path to the event special by ':'s.

```
eventBus.addListener('student:login', () => []);
eventBus.addListener('staff:login', () => []);
```

Sub events can be arbitarily deep this is still valid:

```
eventBus.addListener('I:am:a:very:deeply:nested:sub:event', () => []);
```

By using '**' it is possible to call all subevents at a path.

```
eventBus.addListener('student:**', () => []);
eventBus.emit('student'); // not called
eventBus.emit('student:login'); // called
eventBus.emit('student:logout'); // called
eventBus.emit('student:history:fail') // not called, does not call subevents

eventBus.addListener('student:**:fail')
eventBus.emit('student:login') // not called
eventBus.emit('student:history:fail') // called
```

Using '***' will call all events on that path including subevent.

```
eventBus.addListener('student:***');
eventBus.emit('student:login'); // called
eventBus.emit('student:history'); // called
eventBus.emit('student:history:fail'); // called
```

Adding an event listener returns a new Event object. This object contains the
events path, callback, and id. To remove an event listener call remove on the
event object.

```
const event = eventBus.addListener('staff:login', () => []);
...
event.remove();
```

To emit an event call emit on the event bus, the function takes 2 parameters,
the name of the event, and the values to pass to the callback.

```
eventBus.emit('student:login', { name: 'Jimmy' });
```
