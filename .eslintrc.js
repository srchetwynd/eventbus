module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true,
    },
    extends: ['plugin:jsdoc/recommended', 'prettier'],
    plugins: ['jsdoc', 'prettier'],
    parser: 'babel-eslint',
    rules: {
        'prettier/prettier': 'error',
        'arrow-body-style': 'off',
        'prefer-arrow-callback': 'off',
    },
};
